﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using log4net;

namespace OCR
{
    public class OCRclass
    {
        #region Private Members

        private tessnet2.Tesseract ocr;
        private  int MIN_CONFIDENCE;
        private  int MIN_EXTREME_COFIDENCE;
     
        private Bitmap m_BMPImage;
        private Bitmap m_BMPImage2;
        #endregion

        #region Constructor

        // tessDataPath - the path to files containing the language data for OCR by Tesseract
        // AcceptedCharList - string containing all legal characters to be identified by OCR
        // Language - "eng" for english (only supports english for now)
        // MinConfidence - the minimum OCR detection score to be accepted as good identification
        public OCRclass(string tessDataPath, string AcceptedCharList,int MinConfidence,int MinExtremConfidence,string Language)
        {
          
           MIN_CONFIDENCE = MinConfidence;
           MIN_EXTREME_COFIDENCE = MinExtremConfidence;
           ocr = new tessnet2.Tesseract();
           ocr.SetVariable("tessedit_char_whitelist", AcceptedCharList);
           ocr.Init(tessDataPath, Language, false);
        }

        #endregion

        #region Public Members

       
        // stores the image cropped during GetStringFromLocation method
        public Bitmap BMPImage
        {
            get { return m_BMPImage; }
            set { m_BMPImage = value; }
        }

        // stores the second image, that was cropped with AreaOffset  during GetStringFromLocation method
        public Bitmap BMPImage2
        {
            get { return m_BMPImage2; }
            set { m_BMPImage2 = value; }
        }
        

        #endregion

        #region Public Methods

        public List<string> GetStringFromLocation(Rectangle stringArea,int AreaOffset,String fileName )
        {
            bool Result2IsBetter = false;
            Bitmap img = new Bitmap(fileName);
            cropImage(img, stringArea, "");
            List<string> resultList = new List<string>();
            List<tessnet2.Word> result;
            List<tessnet2.Word> result2;
            try
            {              
                result = ocr.DoOCR(img, stringArea);
                stringArea.Y += AreaOffset;
                cropImage(img, stringArea, "2");
                result2 = ocr.DoOCR(img, stringArea);
            }
            catch (Exception)
            {

                if (resultList.Count == 0)
                {
                    resultList.Add("Fail");
                    resultList.Add("Fail");
                }
                return resultList;
            }
           
            
            if(result2.Count ==1 &&  result.Count == 1)
                if (result2[0].CharList.Count == 9 && result[0].CharList.Count != 9)
                {
                    result = result2;
                    Result2IsBetter = true;
                }
                else
                    if (result2[0].Confidence < result[0].Confidence)
                    {
                        result = result2;
                        Result2IsBetter = true;
                    }
            if (result2.Count < result.Count)
            {
                result = result2;
                Result2IsBetter = true;
            }
            result2 = null;
            if (result.Count > 1)
            {
                for (;  result.Count>1; )
                {
                    result[0].Text = result[0].Text + result[1].Text;
                    result.RemoveAt(1);
                }
            }
            foreach (tessnet2.Word word in result)
            {
                Array myArr = word.Text.ToArray();
                if (word.Confidence < MIN_CONFIDENCE && myArr.Length == 9)
                    resultList.Add(word.Text + "(" + word.Confidence.ToString() + ")(" + Result2IsBetter.ToString() + ")" );
                else if(myArr.Length>9)
                {
                    if (myArr.GetValue(0).ToString() != "1")
                    {
                        if (word.Confidence < MIN_CONFIDENCE)
                            resultList.Add(word.Text.Substring(1, 9) + "(" + word.Confidence.ToString() + ")(" + Result2IsBetter.ToString() + ")");
                    }
                    else
                    {
                        resultList.Add("*" + word.Text.Substring(0, 9) + "(" + word.Confidence.ToString() + ")(" + Result2IsBetter.ToString() + ")");
                    

                    }
                }
           
              
            }
            img.Dispose();
            System.IO.File.Delete(fileName);
            if (resultList.Count == 0)
            {
                foreach (tessnet2.Word word in result)
                {
                    resultList.Add(word.Text);
                }
                resultList.Add("Fail");
            }
            return resultList;
        }

        public List<string> GetStringFromLocationIMajik(Rectangle stringArea, int AreaOffset, Bitmap img)
        {
            bool Result2IsBetter = false;
           
            cropImage(img, stringArea, "");
            List<string> resultList = new List<string>();
            List<tessnet2.Word> result;
            List<tessnet2.Word> result2;
            try
            {
                result = ocr.DoOCR(img, stringArea);
                stringArea.Y += AreaOffset;
                cropImage(img, stringArea, "2");
                result2 = ocr.DoOCR(img, stringArea);
            }
            catch (Exception)
            {

                if (resultList.Count == 0)
                {
                    resultList.Add("Fail");
                    resultList.Add("Fail");
                }
                return resultList;
            }


            if (result2.Count == 1 && result.Count == 1)
                if (result2[0].CharList.Count == 9 && result[0].CharList.Count != 9)
                {
                    result = result2;
                    Result2IsBetter = true;
                }
                else
                    if (result2[0].Confidence < result[0].Confidence)
                    {
                        result = result2;
                        Result2IsBetter = true;
                    }
            if (result2.Count < result.Count)
            {
                result = result2;
                Result2IsBetter = true;
            }
            result2 = null;
            if (result.Count > 1)
            {
                for (; result.Count > 1; )
                {
                    result[0].Text = result[0].Text + result[1].Text;
                    result.RemoveAt(1);
                }
            }
            foreach (tessnet2.Word word in result)
            {
                Array myArr = word.Text.ToArray();
                if (word.Confidence < MIN_CONFIDENCE && myArr.Length == 9)
                    resultList.Add(word.Text + "(" + word.Confidence.ToString() + ")(" + Result2IsBetter.ToString() + ")");
                else if (myArr.Length > 9)
                {
                    if (myArr.GetValue(0).ToString() != "1")
                    {
                        if (word.Confidence < MIN_CONFIDENCE)
                            resultList.Add(word.Text.Substring(1, 9) + "(" + word.Confidence.ToString() + ")(" + Result2IsBetter.ToString() + ")");
                    }
                    else
                    {
                        resultList.Add("*" + word.Text.Substring(0, 9) + "(" + word.Confidence.ToString() + ")(" + Result2IsBetter.ToString() + ")");


                    }
                }


            }
            img.Dispose();
           // System.IO.File.Delete(fileName);
            if (resultList.Count == 0)
            {
                foreach (tessnet2.Word word in result)
                {
                    resultList.Add(word.Text);
                }
                resultList.Add("Fail");
            }
            return resultList;
        }

        public List<string> GetStringsFromAllImage(String fileName, Rectangle stringArea)
        {
            Bitmap img = new Bitmap(fileName);
           
            List<string> resultList = new List<string>();
            List<tessnet2.Word> result;


            result = ocr.DoOCR(img, stringArea);
                foreach (tessnet2.Word word in result)
                {
                   // if(word.Confidence<250)
                        resultList.Add(word.Text);
                }


                return resultList;

        }
        #endregion


        #region Private Methods

        private void cropImage(Image img, Rectangle cropArea, string index)
        {
            Bitmap bmpImage,TempImg;
            TempImg = new Bitmap(img);
            Bitmap bmpCrop;
            try
            {
                bmpCrop = TempImg.Clone(cropArea, TempImg.PixelFormat);
                bmpImage = new Bitmap(bmpCrop.Width / 2, bmpCrop.Height / 2);
                using (Graphics g = Graphics.FromImage((Image)bmpImage))
                 {
                     g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                     g.DrawImage(bmpCrop, 0, 0, bmpCrop.Width / 2, bmpCrop.Height / 2);
                 }
                TempImg.Dispose();
            }
            catch (Exception)
            {

                return;
            }      
           
            if(index == "2")
                m_BMPImage2 = bmpImage;
            else
                m_BMPImage = bmpImage;

           
        }

        #endregion
    }
}
