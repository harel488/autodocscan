﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoDocScan
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
      
        [STAThread]
        static void Main()
        {
            log4net.GlobalContext.Properties["fileDate"] = DateTime.Now.ToString("dd.MM.yyyy_hh.mm.ss'.log'");
            string logPath = "C:\\projects\\AutoDocScan\\AutoDocScan\\log4net.config";
            string lineNumber = "line1";
            log4net.GlobalContext.Properties["lineNumber"] = lineNumber;
            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(logPath));
            ILog m_Logger;
            m_Logger = LogManager.GetLogger("SystemLogs");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            m_Logger.Debug("Run App");
            Application.Run(new frmMain());
        }
    }
}
