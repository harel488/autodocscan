﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PdfToImage;
using OCR;
using System.Data.SqlClient;
using AutoDocScan.Properties;
using System.Drawing.Imaging;
using AForge;
using AForge.Imaging.Filters;
using AForge.Imaging;
using log4net;
using BCrypt;


namespace AutoDocScan
{
    public partial class frmMain : Form
    {
        #region Private Members

        
        private PDFConvert myPdfToImage;
        private OCRclass myOCR;
        private SqlConnection mySQLConnection;
        private  DateTime BaseDate = new DateTime(2013, 3, 19);
        private const int BaseDateInDays = 77513;
        private Dictionary<double, string> JobTypes = new Dictionary<double, string>();
        private SqlCommand command;
        protected ILog m_Logger;

        //configurations members:
        private string AcceptedCharacterList;
        private int MinOCRConfidence;
        private int MinExtremeOCRConfidence;
        private string OCRLanguage;

        // Structure to store JOB data from OTZMA 10 Data Base
        private struct Worker
        {
            public string WorkerName;
            public string WorkerNumber;
            public double WorkerJobType; // this data is a double which corresponds to the frequency of the JOB. this code is then translated through the JobTypes Dictionary
            public string JobGUID;
            public int JobCloseDate;
        }

        #endregion

        #region Constructor

        public frmMain()
        {

            log4net.GlobalContext.Properties["fileDate"] = DateTime.Now.ToString("dd.MM.yyyy_hh.mm.ss'.log'");
            string logPath = "C:\\projects\\AutoDocScan\\AuoDocScan\\log4net.config";
            string lineNumber = "line1";
            log4net.GlobalContext.Properties["lineNumber"] = lineNumber;
            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(logPath));
            m_Logger = LogManager.GetLogger("SystemLogs");
            Configuration.Configuration.Init(new string[] { });
            
            InitializeComponent();

            myPdfToImage = new PDFConvert();

            myPdfToImage.JPEGQuality = 100;
            myPdfToImage.LastPageToConvert = 1;

            myPdfToImage.ResolutionX = Configuration.Configuration.GetValue(@"Config/PDFConvertResolution", 300);
            myPdfToImage.ResolutionY = Configuration.Configuration.GetValue(@"Config/PDFConvertResolution", 300);
            myPdfToImage.OutputFormat = Configuration.Configuration.GetValue(@"Config/PDFConvertOutputFormat", "pngalpha");

            initConfiguration();
            
            myOCR = new OCRclass(Application.StartupPath,               
                AcceptedCharacterList,
                MinOCRConfidence,
                MinExtremeOCRConfidence,
                OCRLanguage);
            fileSystemWatcher1.Path = Configuration.Configuration.GetValue(@"Config/FileWatchPath", @"C:\OCR");
            fileSystemWatcher1.Filter = Configuration.Configuration.GetValue(@"Config/FileFilter", "*.pdf");
            
            //decrypt the passowrd from the configuration file
            string encryptPass = Configuration.Configuration.GetValue(@"Config/power10ConnectionString", @"Data Source=OZMA-10\PW10;Initial Catalog=power10;Persist Security Info=True;User ID=sa;Password=Manager1");
            encryptPass= encryptPass.Substring(encryptPass.IndexOf("Password=") + 9);           
            string decryptPass = Base64Decode(encryptPass);
            string SqlConnectionDetails = Configuration.Configuration.GetValue(@"Config/power10ConnectionString", @"Data Source=OZMA-10\PW10;Initial Catalog=power10;Persist Security Info=True;User ID=sa;Password=Manager1");
            SqlConnectionDetails= SqlConnectionDetails.Substring(0,SqlConnectionDetails.LastIndexOf("Password="));
            SqlConnectionDetails +="Password="+ decryptPass;           
            mySQLConnection = new SqlConnection(SqlConnectionDetails);
            
            fileSystemWatcher1.EnableRaisingEvents = true;
            fileSystemWatcher1.Created += fileSystemWatcher1_Created;
            btnProcess.Text = "P" + "\n" + "R" + "\n" + "O" + "\n" + "C" + "\n" + "E" + "\n" + "S" + "\n" + "S" + "\n\n" + dataGridView1.Rows.Count.ToString();

            fileSystemWatcher1.SynchronizingObject = this;
            this.ShowIcon = false;//for the main form
            this.ShowInTaskbar = false;//for the main form
            this.notifyIcon1.Icon = Properties.Resources.AutoDoc;
            this.notifyIcon1.Visible = true;//for notify icon
            

            FillJobTypesList();

            PrepareSQLInsertCommand();
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
       

        #endregion

        #region Private Methods

        private void initConfiguration()
        {                    
            AcceptedCharacterList = Configuration.Configuration.GetValue(@"Config/AcceptedCharacterList", "0123456789-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,.:'#|");
            MinOCRConfidence= Configuration.Configuration.GetValue(@"Config/MinOCRConfidence", 200);
            MinExtremeOCRConfidence= Configuration.Configuration.GetValue(@"Config/MinExtremeOCRConfidence", 275);
            OCRLanguage= Configuration.Configuration.GetValue(@"Config/OCRLanguage","eng");
        }

        private void PrepareSQLInsertCommand()
        {
            // Create the InsertCommand.
            command = new SqlCommand(
                "INSERT INTO dbo.ENTITYDOCS (EDOENTITYID, EDOPARENTENTITYID,EDOPARENTTYPEID,EDODOCNAME,EDODESCR,EDOLASTUPDATE,EDOUDF15) " +
                "VALUES (@EDOENTITYID, @EDOPARENTENTITYID,@EDOPARENTTYPEID,@EDODOCNAME,@EDODESCR,@EDOLASTUPDATE,@EDOUDF15)", mySQLConnection);

            // Add the parameters for the InsertCommand.
            command.Parameters.Add("@EDOENTITYID", SqlDbType.UniqueIdentifier, 16, "EDOENTITYID");
            command.Parameters.Add("@EDOPARENTENTITYID", SqlDbType.UniqueIdentifier, 16, "EDOPARENTENTITYID");
            command.Parameters.Add("@EDOPARENTTYPEID", SqlDbType.Int, 4, "EDOPARENTTYPEID");
            command.Parameters.Add("@EDODOCNAME", SqlDbType.VarChar, 255, "EDODOCNAME");
            command.Parameters.Add("@EDODESCR", SqlDbType.NVarChar, 200, "EDODESCR");
            command.Parameters.Add("@EDOLASTUPDATE", SqlDbType.Int, 4, "EDOLASTUPDATE");
            command.Parameters.Add("@EDOUDF15", SqlDbType.VarChar, 24, "EDOUDF15");
        }

        private void FillJobTypesList()
        {
            // List of Possible Job Types
            JobTypes.Add(0, "עבודת שבר");

            JobTypes.Add(0.1, "בוצע נוהל שבועי");
            JobTypes.Add(0.2, "בוצע נוהל דו שבועי");

            JobTypes.Add(1, "בוצע נוהל חודשי");
            JobTypes.Add(2, "בוצע נוהל דו חודשי");
            JobTypes.Add(3, "בוצע נוהל תלת חודשי");

            JobTypes.Add(6, "בוצע נוהל חצי שנתי");
            JobTypes.Add(12, "בוצע נוהל שנתי");
            JobTypes.Add(24, "בוצע נוהל דו שנתי");

            JobTypes.Add(36, "בוצע נוהל תלת שנתי");
            JobTypes.Add(60, "בוצע נוהל חמש שנתי");
        }

        //function to add a row to the datagridview 
        private void AddRow(string Fname, string JobNum, string OCRscore, Bitmap Bimg)
        {
            int i = 0;

            Worker myWorker;
            if (OCRscore != "")
            {

                // Get details from OTZMA 10 data base according to the Job Number extracted by OCR from image
                myWorker = GetWorkerDetails(JobNum);

                // details were successfully received from OTZMA 10 DB
                if (myWorker.WorkerName != null)
                {
                    int j = dataGridView1.Rows.Add(Fname, OCRscore,
                        myWorker.WorkerNumber,
                        myWorker.WorkerName,
                        JobTypes[myWorker.WorkerJobType],
                        JobNum,
                        Bimg,
                        myWorker.JobGUID,
                        myWorker.JobCloseDate);

                    dataGridView1.Rows[j].DefaultCellStyle.BackColor = Color.LightGreen;
                }
                else
                {
                    // details were not received from OTZMA 10 DB
                    try
                    {
                        i = dataGridView1.Rows.Add(Fname, OCRscore, "", "", "", JobNum, Bimg);

                    }
                    catch (Exception)
                    {

                        i = dataGridView1.Rows.Add(Fname, OCRscore, "", "", "", JobNum, null);
                    }

                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                }
            }
            else
            {
                // No string could be extracted from Image
                try
                {
                    i = dataGridView1.Rows.Add(Fname, OCRscore, "", "", "", JobNum, Bimg);


                }
                catch (Exception)
                {

                    i = dataGridView1.Rows.Add(Fname, OCRscore, "", "", "", JobNum, null);
                }

                dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Red;
            }

            notifyIcon1.Text = "Auto Doc Scan \n" + dataGridView1.Rows.Count.ToString() + " Documents Scanned";
            notifyIcon1.BalloonTipText = "Auto Doc Scan \n" + " New Documents Have Been Scanned";
            notifyIcon1.ShowBalloonTip(2000);

        }

        private Worker GetWorkerDetails(string JobNumber)
        {
            
            char[] splitters = { '-' };
            Worker myWorker = new Worker();
            string[] splitarray = JobNumber.Split(splitters);
            if (splitarray.Length < 2)
                return myWorker;
            JobNumber = splitarray[0] + splitarray[1];
            m_Logger.Debug("Start GetWorkerDetails" + " " + "JobNumber"+ " " + JobNumber);
            mySQLConnection.Open();
            m_Logger.Debug("mySQLConnection.Open");
            String tmpStr = "SELECT     EXECUTER.EXEID, EXECUTER.EXEEXETYPE, EXECUTER.EXEDESCR, EXECUTER.EXEGROUP, JOB_OPEN.JOBNO, JOB_OPEN.JOBFREQ, JOB_OPEN.JOBENTITYID,JOB_OPEN.JOBCLOSDATE " +
            "FROM         EXECUTER INNER JOIN " +
                      " JOB_OPEN ON EXECUTER.EXEID = JOB_OPEN.JOBEXEID " +
            "WHERE JOB_OPEN.JOBNO = '" + JobNumber + "'";

            using (SqlDataAdapter a = new SqlDataAdapter(tmpStr, mySQLConnection))
            {

                DataTable t = new DataTable();
                if (t == null)
                    m_Logger.Error("No Data from sql query");
                try
                {
                    a.Fill(t);
                }
                catch (Exception)
                {
                    mySQLConnection.Close();
                    return myWorker;
                }

                // 4
                if (t.Rows.Count > 0)
                {

                    myWorker.WorkerName = t.Rows[0]["EXEDESCR"].ToString();
                    myWorker.WorkerNumber = t.Rows[0]["EXEID"].ToString();
                    myWorker.WorkerJobType = (double)t.Rows[0]["JOBFREQ"];
                    myWorker.JobGUID = t.Rows[0]["JOBENTITYID"].ToString();
                    myWorker.JobCloseDate = (int)t.Rows[0]["JOBCLOSDATE"];
                }
            }
            mySQLConnection.Close();
            m_Logger.Debug("mySQLConnection.Close");
            m_Logger.Debug("End GetWorkerDetails" + " " +"JobNumber"+" " + JobNumber);
            return myWorker;
        }

        private void UpdateDataGridRow(int rowIndex, Worker myWorker)
        {
            m_Logger.Debug("UpdateDataGridRow");
            if (myWorker.WorkerName != null)
            {

                dataGridView1.Rows[rowIndex].Cells[2].Value = myWorker.WorkerNumber;
                dataGridView1.Rows[rowIndex].Cells[3].Value = myWorker.WorkerName;
                dataGridView1.Rows[rowIndex].Cells[4].Value = JobTypes[myWorker.WorkerJobType];
                dataGridView1.Rows[rowIndex].Cells[7].Value = myWorker.JobGUID;
                dataGridView1.Rows[rowIndex].Cells[8].Value = myWorker.JobCloseDate;
                dataGridView1.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LightGreen;
            }
        }

        private SqlCommand PrpepareDataGridRowForDBEntry(DataGridViewRow item)
        {
            bool isExist = false;
            try
            {
                System.IO.File.Move(Configuration.Configuration.GetValue(@"Config/FileWatchPath", @"C:\OCR") + "\\" + item.Cells[0].Value.ToString(),
                                Configuration.Configuration.GetValue(@"Config/ileTransferPath", @"C:\OCR\Moved") + "\\" + item.Cells[3].Value.ToString() + "\\" + item.Cells[5].Value.ToString() + ".PDF");
            }
            catch(Exception ex)
            {
                if (ex.Message.Contains("The file exist"))
                {
                    MessageBox.Show(ex.Message);
                    m_Logger.Error("The file " + item.Cells[0].Value.ToString() + " already Exists in System");
                    isExist = true;
                }
            }
            if (!isExist)
            {
                command.Parameters["@EDOENTITYID"].Value = System.Guid.NewGuid();
                command.Parameters["@EDOPARENTENTITYID"].Value = new Guid(item.Cells[7].Value.ToString());
                command.Parameters["@EDOPARENTTYPEID"].Value = 1;
                command.Parameters["@EDODOCNAME"].Value = Configuration.Configuration.GetValue(@"Config/ileTransferPath", @"C:\OCR\Moved") + "\\" + item.Cells[3].Value.ToString() + "\\" + item.Cells[5].Value.ToString() + ".PDF";

                TimeSpan ts = (DateTime.Now - BaseDate);


                command.Parameters["@EDOLASTUPDATE"].Value = BaseDateInDays + (int)ts.TotalDays;
                double NumDays = (int)item.Cells[8].Value - BaseDateInDays;
                DateTime tempDate = BaseDate;
                command.Parameters["@EDODESCR"].Value = item.Cells[4].Value + " " + tempDate.AddDays(NumDays).ToShortDateString();
                command.Parameters["@EDOUDF15"].Value = "JOB";
            }
            return command;
        }

      

        #endregion

        #region GUI event handlers

        private void fileSystemWatcher1_Created(object sender, System.IO.FileSystemEventArgs e)
        {
           // Convert the New created PDF file to a PNG image
            lock (myPdfToImage)
            {
                myPdfToImage.Convert(e.FullPath, e.FullPath+".png");
      
            }

            // Get the JOB Number string by OCR from the PNG image and then erase the image file
            // the area to crop from the image is defined in Settings
            List<string> resList = myOCR.GetStringFromLocation(new Rectangle(Configuration.Configuration.GetValue(@"Config/CropAreaLeft", 1750),
                Configuration.Configuration.GetValue(@"Config/CropAreaTop", 600), Configuration.Configuration.GetValue(@"Config/CropAreaWidth", 290),
                 Configuration.Configuration.GetValue(@"Config/CropAreaHeight", 110)), Configuration.Configuration.GetValue(@"Config/RectangleAreaOffset", -25), e.FullPath + ".png");

 
            if (resList.Count == 1)
            {
                
                char[] splitters = { '(', ')' };
                string[] splitarray = resList[0].Split(splitters);
                string i = "";
                if (splitarray[3] == "true")
                    i = "2";
                else
                    i = "";
                if(i=="2")
                    AddRow(e.Name, splitarray[0], splitarray[1], myOCR.BMPImage2);
                else
                    AddRow(e.Name, splitarray[0], splitarray[1], myOCR.BMPImage);
            }
            else
            {
                AddRow(e.Name, "", "", myOCR.BMPImage);
            }

        }
       
        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            // attempt to get data from OTZMA 10 DB manually
          
            UpdateDataGridRow(e.RowIndex,GetWorkerDetails(dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()));                  
        }
     
        private void btnProcess_Click(object sender, EventArgs e)
        {
            m_Logger.Debug("Start btnProcess_Click");
            int NumDocsProcessed = 0;
            mySQLConnection.Open();
            List<DataGridViewRow> RowsToDelete = new List<DataGridViewRow>();
            foreach (DataGridViewRow item in dataGridView1.Rows)
            {
                if (item.DefaultCellStyle.BackColor != Color.Red)
                {
                    try
                    {
                        
                        if(Configuration.Configuration.GetValue(@"Config/EnterToDB",true)) 
                        {
                            m_Logger.Debug("Settings.Default.EnterToDB");
                            PrpepareDataGridRowForDBEntry(item).ExecuteNonQuery();
                        }
                        else
                        {
                            m_Logger.Debug("!Settings.Default.EnterToDB");
                            PrpepareDataGridRowForDBEntry(item);
                        }
                       
                        RowsToDelete.Add(item);
                        NumDocsProcessed++;
                    }
                    catch (Exception ex)
                    {
                        
                        if(ex.Message.Contains("path"))
                        {
                            MessageBox.Show("Could not find path: " + Configuration.Configuration.GetValue(@"Config/FileTransferPath", @"C:\OCR\Moved") + "\\" + item.Cells[3].Value.ToString() + "\nDocument will not be processed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }

                        if (ex.Message.Contains("already"))
                        {
                            MessageBox.Show("File already Exists: " +  item.Cells[5].Value.ToString() + "\nDocument will not be processed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        m_Logger.Error(ex.Message);
                    }
                }
                m_Logger.Debug("End btnProcess_Click");
            }

            while (RowsToDelete.Count>0)
            {
                dataGridView1.Rows.Remove(RowsToDelete[0]);
                RowsToDelete.RemoveAt(0);
            }
            mySQLConnection.Close();
            notifyIcon1.BalloonTipText = "Auto Doc Scan \n" + NumDocsProcessed.ToString() + " Documents Have been Entered to OTZMA 10";
            notifyIcon1.Text = "Auto Doc Scan \n" + dataGridView1.Rows.Count.ToString() + " Documents Scanned";
            notifyIcon1.ShowBalloonTip(2000);
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
                this.Hide();
        }

        private void notifyIcon1_MouseUp(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void processToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnProcess_Click(sender, e);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnClear_Click(sender, e);
        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            System.Diagnostics.Process.Start(Configuration.Configuration.GetValue(@"Config/FileWatchPath", @"C:\OCR") + "\\" + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            btnProcess.Text = "P" + "\n" + "R" + "\n" + "O" + "\n" + "C" + "\n" + "E" + "\n" + "S" + "\n" + "S" + "\n\n" + dataGridView1.Rows.Count.ToString();
        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            btnProcess.Text = "P" + "\n" + "R" + "\n" + "O" + "\n" + "C" + "\n" + "E" + "\n" + "S" + "\n" + "S" + "\n\n" + dataGridView1.Rows.Count.ToString();
        }

        #endregion


        #region RnD

        //private void button1_Click(object sender, EventArgs e)
        //{
        //   // var bitmap = new Bitmap("c:\\7331-4010-0 .PDF.png");
        // //   ApplyFilter(new YCbCrExtractChannel(YCbCr.CbIndex),bitmap);
        //    List<string> fullist = myOCR.GetStringsFromAllImage("c:\\test-training.PDF.png",new Rectangle()); //new Rectangle(2350,2650,2150,800));
        //    foreach (string item in fullist)
        //    {
        //        listBox1.Items.Add(item);
        //    }
        //  //  bitmap.Save("C:\\redtedtimage2.bmp");
        //}

        private void ApplyFilter(IFilter filter,Bitmap image)
        {

            // apply filter to the image
            Bitmap newImage = filter.Apply(image);
            newImage.Save("C:\\redtedtimage2.bmp");
            newImage.Dispose();
        }


        #endregion

    }
}
