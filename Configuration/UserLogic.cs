﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configuration
{
    public static class UserLogic
    {
        public enum UserAuthorization
        {
            Operator,
            ShiftManager,
            Administartor
        }

        private static string user;
        private static UserAuthorization userAuthorization;

        public static void Init(string[] args)
        {
            userAuthorization = UserAuthorization.Operator;
            if (args.Length == 0)
            {
                user = "DefaultUser";                
            }
            else
            {
                user = args[0];
                JToken userAuthorizationObj = Configuration.GetToken("userAuthorization");
                List<string> adminstratorsList = (List<string>)((JArray)userAuthorizationObj["admin"]).ToObject<IEnumerable<string>>();
                if (adminstratorsList.Contains(user))
                {
                    userAuthorization = UserAuthorization.Administartor;
                }
                else
                {
                    List<string> shiftManagerList = (List<string>)((JArray)userAuthorizationObj["shiftManager"]).ToObject<IEnumerable<string>>();
                    if (shiftManagerList.Contains(user))
                    {
                        userAuthorization = UserAuthorization.ShiftManager;
                    }
                }

            }
            
        }

        public static string User
        {
            get
            {
                return user;
            }
        }

        public static UserAuthorization UserAuthorizationLevel
        {
            get
            {
                return userAuthorization;
            }
        }
    }
}
