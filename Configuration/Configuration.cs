﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configuration
{
    public static class Configuration
    {
        static JObject configObj;

        public static void Init(string[] args)
        {
            string configPath = @"C:\Projects\AutoDocScan\auto_doc_scan_config.json"; // Move to registery
            configObj = JObject.Parse(File.ReadAllText(configPath));
            UserLogic.Init(args);
        }

        public static void Add(string key, string value)
        {
            configObj[key] = value;
        }

        public static JToken GetToken(string token)
        {
            return configObj[token];
        }

        public static Dictionary<string, string[]> GetDict(string longPath)
        {
            try
            {
                JToken jt = BrowseInPath(longPath);
                IDictionary<string, JToken> objToken = (JObject)jt;
                Dictionary<string, string[]> dictionary = objToken.ToDictionary(pair => pair.Key, pair => (string[])pair.Value.ToObject<string[]>());
                return dictionary;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Dictionary<string, string> GetDict2(string longPath)
        {
            try
            {
                JToken jt = BrowseInPath(longPath);
                IDictionary<string, JToken> objToken = (JObject)jt;
                Dictionary<string, string> dictionary = objToken.ToDictionary(pair => pair.Key, pair => (string)pair.Value.ToObject<string>());
                return dictionary;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Single> GetArray(string longPath)
        {
            try
            {
                JToken jt = BrowseInPath(longPath);
                return jt.Select(s => (Single)s).ToList();
            }
            catch (Exception)
            {
                return new List<Single>();
            }
        }

        public static string GetValue(string longPath, string def)
        {
            try
            {
                JToken jt = BrowseInPath(longPath);
                return jt.ToString();
            }
            catch (Exception)
            {
                return def;
            }           
        }



        public static int GetValue(string longPath, int def)
        {
            try
            {
                JToken jt = BrowseInPath(longPath);
                return Convert.ToInt32(jt);
            }
            catch (Exception)
            {
                return def;
            }
        }

        public static double GetValue(string longPath, double def)
        {
            try
            {
                JToken jt = BrowseInPath(longPath);
                return Convert.ToDouble(jt);
            }
            catch (Exception)
            {
                return def;
            }
        }

        public static float GetValue(string longPath, float def)
        {
            try
            {
                JToken jt = BrowseInPath(longPath);
                return float.Parse(jt.ToString());
            }
            catch (Exception)
            {
                return def;
            }
        }

        public static byte GetValue(string longPath, byte def)
        {
            try
            {
                JToken jt = BrowseInPath(longPath);
                return Convert.ToByte(jt);
            }
            catch (Exception)
            {
                return def;
            }
        }

        public static bool GetValue(string longPath, bool def)
        {
            try
            {
                JToken jt = BrowseInPath(longPath);
                return Convert.ToBoolean(jt);
            }
            catch (Exception)
            {
                return def;
            }
        }
        
        private static JToken BrowseInPath(string longPath, bool removeLastPath = false)
        {
            string[] paths = longPath.Split('/');
            if (removeLastPath)
                paths = paths.Take(paths.Count() - 1).ToArray();
            JToken jt = GetToken(paths[0]);
            if (paths.Length > 1)
            {
                for (int i = 1; i < paths.Length; i++)
                {
                    jt = jt[paths[i]];
                }
            }

            return jt;
        }

        public static void SetValue(string longPath, bool def)
        {
            // assume that only the last part of the longPath should be created
            try
            {
                JToken jt = BrowseInPath(longPath, true);
                string[] paths = longPath.Split('/');
                ((JObject)jt).Remove(paths[paths.Count() - 1]);

                ((JObject)jt).Add(paths[paths.Count() - 1], def);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SetValue(string longPath, string def)
        {
            // assume that only the last part of the longPath should be created
            try
            {
                JToken jt = BrowseInPath(longPath,true);
                string[] paths = longPath.Split('/');
                ((JObject)jt).Remove(paths[paths.Count() - 1]);

                ((JObject)jt).Add(paths[paths.Count() - 1], def);
            }
            catch (Exception ex)
            {
            }
        }
    }
}
